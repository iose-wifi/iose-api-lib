'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./modules/config');

var _admin = require('./modules/admin');

var _admin2 = _interopRequireDefault(_admin);

var _employee = require('./modules/employee');

var _employee2 = _interopRequireDefault(_employee);

var _circuit = require('./modules/circuit');

var _circuit2 = _interopRequireDefault(_circuit);

var _unity = require('./modules/unity');

var _unity2 = _interopRequireDefault(_unity);

var _group = require('./modules/group');

var _group2 = _interopRequireDefault(_group);

var _tariffs = require('./modules/tariffs');

var _tariffs2 = _interopRequireDefault(_tariffs);

var _agent = require('./modules/agent');

var _agent2 = _interopRequireDefault(_agent);

var _flag = require('./modules/flag');

var _flag2 = _interopRequireDefault(_flag);

var _measures = require('./modules/measures');

var _measures2 = _interopRequireDefault(_measures);

var _household_appliance = require('./modules/household_appliance');

var _household_appliance2 = _interopRequireDefault(_household_appliance);

var _cep = require('./modules/cep');

var _cep2 = _interopRequireDefault(_cep);

var _notifications = require('./modules/notifications');

var _notifications2 = _interopRequireDefault(_notifications);

var _tips = require('./modules/tips');

var _tips2 = _interopRequireDefault(_tips);

var _client = require('./modules/client');

var _client2 = _interopRequireDefault(_client);

var _logs = require('./modules/logs');

var _logs2 = _interopRequireDefault(_logs);

var _alarms = require('./modules/alarms');

var _alarms2 = _interopRequireDefault(_alarms);

var _circuitThreePhase = require('./modules/circuitThreePhase');

var _circuitThreePhase2 = _interopRequireDefault(_circuitThreePhase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var IoseApiLib = {
  setToken: _config.setToken,
  showHeader: _config.showHeader,
  setEnv: _config.setEnv,
  showUrl: _config.showUrl,
  Admin: _admin2.default,
  Employee: _employee2.default,
  Circuit: _circuit2.default,
  Unity: _unity2.default,
  Group: _group2.default,
  Tariff: _tariffs2.default,
  Agent: _agent2.default,
  Flag: _flag2.default,
  Measures: _measures2.default,
  HouseholdAppliance: _household_appliance2.default,
  Cep: _cep2.default,
  Notifications: _notifications2.default,
  Tips: _tips2.default,
  Client: _client2.default,
  Logs: _logs2.default,
  Alarms: _alarms2.default,
  CircuitThreePhase: _circuitThreePhase2.default
};

exports.default = IoseApiLib;