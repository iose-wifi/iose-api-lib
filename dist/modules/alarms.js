'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Alarms = {};

/**
 * Get all alarms of a circult between start date and end date
 * @param {String} uuidCircuit - circuit identifier
 * @param {String} startDate - start date
 * @param {String} endDate - end date
 * @param {Object} pageCode - code of the page
 * @param {Number} pageSize - size of the page
 * @returns {Promise}
 */
Alarms.getAllAlarmsByCircuitBetweenDates = async function (uuidCircuit, startDate, endDate) {
  var pageCode = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '0';
  var pageSize = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 300;

  var params = {
    uuid_circuit: uuidCircuit,
    start_date: startDate,
    end_date: endDate,
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getallalarmsbycircuitbetweendates', {
    params: params
  });

  return response.data;
};

exports.default = Alarms;