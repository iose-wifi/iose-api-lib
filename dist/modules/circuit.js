'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Circuit = {};

/**
 * Remove properties from a circuit
 * @param {String} uuidCircuit - circuit identifier
 * @returns {Promise}
 */
Circuit.removePropertyCircuit = async function (uuidCircuit) {
  var params = {
    uuid_circuit: uuidCircuit
  };
  var response = await _config2.default.delete('removepropertycircuit', {
    data: params
  });
  return response.data;
};

/**
 * Delete a Circuit
 * @param {String} uuidCircuit - circuit identifier
 * @returns {Promise}
 */
Circuit.deleteCircuit = async function (uuidCircuit) {
  var params = {
    uuid_circuit: uuidCircuit
  };
  var response = await _config2.default.delete('deletecircuit', { data: params });
  return response.data;
};

/**
 * Update circuit informations
 * @param {String} uuidCircuit - circuit identifier
 * @param {String} name - circuit new name
 * @param {String} description -circuit new description
 * @returns {Promise}
 */
Circuit.updateCircuit = async function (uuidCircuit, name, description) {
  var params = {
    uuid_circuit: uuidCircuit,
    name: name,
    description: description
  };
  var response = await _config2.default.put('updatecircuit', params);
  return response.data;
};

/**
 * Update Circuit owners (unity, group)
 * @param {String} uuidCircuit - circuit identifier
 * @param {String} uuidUnity - unity identifier
 * @param {String} uuidGroup - group identifier
 * @returns {Promise}
 */
Circuit.updatePropertyCircuit = async function (uuidCircuit, uuidUnity, uuidGroup) {
  var params = {
    uuid_circuit: uuidCircuit,
    uuid_unity: uuidUnity,
    uuid_group: uuidGroup
  };
  var response = await _config2.default.put('updatepropertycircuit', params);
  return response.data;
};

/**
 * Get all new circuits of a client
 * @param {*} uuidClient - client identifier
 * @param {*} pageCode - (optional) code of page - default: '0'
 * @param {*} pageSize - (optional) size of page - default: 300
 * @returns {Promise}
 */
Circuit.getNewCircuit = async function (uuidClient) {
  var pageCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0';
  var pageSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;

  var params = {
    uuid_client: uuidClient,
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getnewcircuit', { params: params });
  return response.data;
};

/**
 * get all circuits of an unity
 * @param {String} uuidUnity -
 * @param {*} pageCode - (optional) code of page - default: '0'
 * @param {*} pageSize - (optional) size of page - default: 300
 * @returns
 */
Circuit.getAllCircuit = async function (uuidUnity) {
  var pageCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0';
  var pageSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;

  var params = {
    uuid_unity: uuidUnity,
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getallcircuit', { params: params });
  return response.data;
};

/**
 * get all circuits of a group
 * @param {String} uuidGroup -
 * @param {*} pageCode - (optional) code of page - default: '0'
 * @param {*} pageSize - (optional) size of page - default: 300
 * @returns
 */
Circuit.getAllCircuitsOfGroup = async function (uuidGroup) {
  var pageCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0';
  var pageSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;

  var params = {
    uuid_group: uuidGroup,
    page_code: pageCode,
    page_size: pageSize
  };
  var response = await _config2.default.get('getallcircuitsofgroup', { params: params });
  return response.data;
};

/**
 * Trigger a circuit
 * @param {String} uuidCircuit - circuit identifier
 * @param {Boolean} stateActuation - new state of circuit
 * @returns
 */
Circuit.triggerCircuit = async function (uuidCircuit, stateActuation, uuidEmployee, uuidAdmin, justification) {
  var params = {
    uuid_circuit: uuidCircuit,
    state_actuation: stateActuation,
    uuid_employee: uuidEmployee,
    uuid_admin: uuidAdmin,
    justification: justification
  };
  var response = await _config2.default.put('actioncircuit', params);
  return response.data;
};

/**
 * get a circuit by QR code
 * @param {String} code_module - Qr code scanned of module
 * @returns
 */
Circuit.getCircuitByCode = async function (code_module) {
  var params = {
    code_module: code_module
  };

  var response = await _config2.default.get('getcircuitbycode', { params: params });
  return response;
};

exports.default = Circuit;