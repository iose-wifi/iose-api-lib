'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Tips = {};

/**
 * Get a random tip
 * @returns {Promise}
 */
Tips.getRandom = async function () {
  var response = await _config2.default.get('getrandomtip');
  return response.data;
};

exports.default = Tips;