'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.showUrl = exports.showHeader = exports.setToken = exports.setEnv = undefined;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PATH_PRODUCTION = 'https://0kp0y3fg13.execute-api.us-east-1.amazonaws.com/prod/';
var PATH_DEVELOPMENT = 'https://vsnhd5rs46.execute-api.us-west-2.amazonaws.com/dev/';

_axios2.default.defaults.baseURL = process.env.REACT_APP_ENV === 'prod' ? PATH_PRODUCTION : PATH_DEVELOPMENT;

var setEnv = exports.setEnv = function setEnv() {
  var env = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'dev';

  var path = env === 'prod' || process.env.REACT_APP_ENV === 'prod' ? PATH_PRODUCTION : PATH_DEVELOPMENT;
  _axios2.default.defaults.baseURL = path;
};

var setToken = exports.setToken = function setToken(token) {
  _axios2.default.defaults.headers.common.Authorization = token;
};

var showHeader = exports.showHeader = function showHeader() {
  console.log(_axios2.default.defaults.headers.common);
};

var showUrl = exports.showUrl = function showUrl() {
  console.log(_axios2.default.defaults.baseURL);
};

exports.default = _axios2.default;