'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Agent = {};

/**
 * Get all agents existents
 * @returns {Promise}
 */
Agent.getAllAgents = async function () {
  var response = await _config2.default.get('getallagents');
  return response.data;
};

/**
 * Get filter all agents existents
 * @returns {Promise}
 */
Agent.getFilterAgents = async function (name_agent, subgroup, modality, acessor, class_agent, subclass) {
  var params = {
    name_agent: name_agent,
    subgroup: subgroup,
    modality: modality,
    acessor: acessor,
    class_agent: class_agent,
    subclass: subclass
  };
  var response = await _config2.default.get('getfilteragents', { params: params });
  return response.data;
};

exports.default = Agent;