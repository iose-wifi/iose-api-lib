'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Tariff = {};

/**
 * Get tarrifs data of an unity
 * @param {String} uuidUnity - unity identifier
 * @param {Array} circuits - circuits array
 * @param {String} startInterval - start date
 * @param {String} endInterval - end date
 * @returns
 */
Tariff.getTariffByUnity = async function (uuidUnity, circuits, startInterval, endInterval) {
  var circuitsString = JSON.stringify(circuits);

  var params = {
    uuid_unity: uuidUnity,
    circuits: circuitsString,
    startInterval: startInterval,
    endInterval: endInterval
  };
  var response = await _config2.default.get('getTariffBasedUnityType', { params: params });
  return response.data;
};

/**
 * Get tarrifs data of an unity with periods
 * @param {String} uuidUnity - unity identifier
 * @param {Array} circuits - circuits array
 * @param {String} startInterval - start date
 * @param {String} endInterval - end date
 * @param {String} modality - end date
 * @returns
 */
Tariff.getTariffBasedUnityTypeAndPeriod = async function (uuidUnity, circuits, startInterval, endInterval, modality) {
  var circuitsString = JSON.stringify(circuits);

  var params = {
    uuid_unity: uuidUnity,
    circuits: circuitsString,
    startInterval: startInterval,
    endInterval: endInterval,
    modality: modality
  };
  var response = await _config2.default.get('getTariffBasedUnityTypeAndPeriod', {
    params: params
  });
  return response.data;
};

exports.default = Tariff;