'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CircuitThreePhase = {};

/**
 * Create a circuitThreePhase
 * @param {String} name - circuit three-phase name
 * @param {String} description - circuit three-phase  description
 * @param {String} identifier_phase_a - uuid circuit phase A
 * @param {String} identifier_phase_b - uuid circuit phase B
 * @param {String} identifier_phase_c - uuid circuit phase C
 * @param {String} uuid_group - uuid group of circuit
 * @param {String} uuid_unity - uuid unity of circuit
 * @param {String} uuid_client - uuid client of circuit
 * @return {Promise}
 */
CircuitThreePhase.create = async function (name, description, identifier_phase_a, identifier_phase_b, identifier_phase_c, uuid_group, uuid_unity, uuid_client) {

  var data = {
    name: name,
    description: description,
    identifier_phase_a: identifier_phase_a,
    identifier_phase_b: identifier_phase_b,
    identifier_phase_c: identifier_phase_c,
    uuid_group: uuid_group,
    uuid_unity: uuid_unity,
    uuid_client: uuid_client
  };

  var response = await _config2.default.post('insertThreePhaseCircuit', data);
  return response.data;
};

/**
 * Get all circuits three-phase by group
 * @param {String} uuidGroup - uuid group 
 * @returns {Promise}
 */
CircuitThreePhase.get = async function (uuidGroup) {

  var params = {
    uuid_group: uuidGroup
  };

  var response = await _config2.default.get('getThreePhaseCircuitsByGroup', { params: params });
  return response.data;
};

/**
 * Update a circuitThreePhase
 * @param {String} uuid_three_phase_circuit - circuit three-phase uuid
 * @param {String} name - circuit three-phase name
 * @param {String} description - circuit three-phase  description
 * @returns {Promise}
 */
CircuitThreePhase.update = async function (uuid_three_phase_circuit, name, description) {

  var params = {
    uuid_three_phase_circuit: uuid_three_phase_circuit,
    name: name,
    description: description
  };

  var response = await _config2.default.put('updateThreePhaseCircuit', params);
  return response.data;
};

/**
 * Delete a circuitThreePhase
 * @param {String} uuid_three_phase_circuit - circuit three-phase uuid
 * @param {String} identifier_phase_a - uuid circuit phase A
 * @param {String} identifier_phase_b - uuid circuit phase B
 * @param {String} identifier_phase_c - uuid circuit phase C
 * @returns {Promise}
 */
CircuitThreePhase.delete = async function (uuid_three_phase_circuit, identifier_phase_a, identifier_phase_b, identifier_phase_c) {

  var params = {
    uuid_three_phase_circuit: uuid_three_phase_circuit,
    identifier_phase_a: identifier_phase_a,
    identifier_phase_b: identifier_phase_b,
    identifier_phase_c: identifier_phase_c
  };

  var response = await _config2.default.delete('deleteThreePhaseCircuit', { data: params });
  return response.data;
};

exports.default = CircuitThreePhase;