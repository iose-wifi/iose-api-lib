import axios from './config';

const Circuit = {};

/**
 * Remove properties from a circuit
 * @param {String} uuidCircuit - circuit identifier
 * @returns {Promise}
 */
Circuit.removePropertyCircuit = async (uuidCircuit) => {
  const params = {
    uuid_circuit: uuidCircuit,
  };
  const response = await axios.delete('removepropertycircuit', {
    data: params,
  });
  return response.data;
};

/**
 * Delete a Circuit
 * @param {String} uuidCircuit - circuit identifier
 * @returns {Promise}
 */
Circuit.deleteCircuit = async (uuidCircuit) => {
  let params = {
    uuid_circuit: uuidCircuit,
  };
  const response = await axios.delete('deletecircuit', { data: params });
  return response.data;
};

/**
 * Update circuit informations
 * @param {String} uuidCircuit - circuit identifier
 * @param {String} name - circuit new name
 * @param {String} description -circuit new description
 * @returns {Promise}
 */
Circuit.updateCircuit = async (uuidCircuit, name, description) => {
  const params = {
    uuid_circuit: uuidCircuit,
    name: name,
    description: description,
  };
  const response = await axios.put('updatecircuit', params);
  return response.data;
};

/**
 * Update Circuit owners (unity, group)
 * @param {String} uuidCircuit - circuit identifier
 * @param {String} uuidUnity - unity identifier
 * @param {String} uuidGroup - group identifier
 * @returns {Promise}
 */
Circuit.updatePropertyCircuit = async (uuidCircuit, uuidUnity, uuidGroup) => {
  const params = {
    uuid_circuit: uuidCircuit,
    uuid_unity: uuidUnity,
    uuid_group: uuidGroup,
  };
  const response = await axios.put('updatepropertycircuit', params);
  return response.data;
};

/**
 * Get all new circuits of a client
 * @param {*} uuidClient - client identifier
 * @param {*} pageCode - (optional) code of page - default: '0'
 * @param {*} pageSize - (optional) size of page - default: 300
 * @returns {Promise}
 */
Circuit.getNewCircuit = async (uuidClient, pageCode = '0', pageSize = 300) => {
  const params = {
    uuid_client: uuidClient,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getnewcircuit', { params: params });
  return response.data;
};

/**
 * get all circuits of an unity
 * @param {String} uuidUnity -
 * @param {*} pageCode - (optional) code of page - default: '0'
 * @param {*} pageSize - (optional) size of page - default: 300
 * @returns
 */
Circuit.getAllCircuit = async (uuidUnity, pageCode = '0', pageSize = 300) => {
  const params = {
    uuid_unity: uuidUnity,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getallcircuit', { params: params });
  return response.data;
};

/**
 * get all circuits of a group
 * @param {String} uuidGroup -
 * @param {*} pageCode - (optional) code of page - default: '0'
 * @param {*} pageSize - (optional) size of page - default: 300
 * @returns
 */
Circuit.getAllCircuitsOfGroup = async (
  uuidGroup,
  pageCode = '0',
  pageSize = 300
) => {
  const params = {
    uuid_group: uuidGroup,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getallcircuitsofgroup', { params: params });
  return response.data;
};

/**
 * Trigger a circuit
 * @param {String} uuidCircuit - circuit identifier
 * @param {Boolean} stateActuation - new state of circuit
 * @returns
 */
Circuit.triggerCircuit = async (
  uuidCircuit,
  stateActuation,
  uuidEmployee,
  uuidAdmin,
  justification
) => {
  const params = {
    uuid_circuit: uuidCircuit,
    state_actuation: stateActuation,
    uuid_employee: uuidEmployee,
    uuid_admin: uuidAdmin,
    justification: justification
  };
  const response = await axios.put('actioncircuit', params);
  return response.data;
};

/**
 * get a circuit by QR code
 * @param {String} code_module - Qr code scanned of module
 * @returns
 */
 Circuit.getCircuitByCode = async (
  code_module,
) => {
  const params = {
    code_module: code_module,
  };

  const response = await axios.get('getcircuitbycode', { params: params });
  return response;
};

export default Circuit;
