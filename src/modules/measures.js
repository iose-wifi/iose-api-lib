import axios from './config';

const Measures = {};

/**
 * Get last measure of the circuit between start and end date
 * @param {String} uuidCircuit - circuit identifier
 * @param {String} startDate - start date
 * @param {String} endDate - end date
 * @returns
 */
Measures.getLastMeasures = async (uuidCircuit, startDate, endDate) => {
  const params = {
    uuid_circuit: uuidCircuit,
    start_date: startDate,
    end_date: endDate,
  };
  const response = await axios.get('getlastmeasure', {params: params});
  return response.data;
};

/**
 * Get all measures of a circult between start date and end date
 * @param {String} uuidCircuit - circuit identifier
 * @param {String} startDate - start date
 * @param {String} endDate - end date
 * @param {Object} pageCode - code of the page
 * @param {Number} pageSize - size of the page
 * @returns {Promise}
 */
Measures.getAllMeasures = async (
  uuidCircuit,
  startDate,
  endDate,
  pageCode = '0',
  pageSize = 300,
) => {
  const params = {
    uuid_circuit: uuidCircuit,
    start_date: startDate,
    end_date: endDate,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getallmeasure', {params: params});

  return response.data;
};

export default Measures;
