import axios from './config';

const Agent = {};

/**
 * Get all agents existents
 * @returns {Promise}
 */
Agent.getAllAgents = async () => {
  const response = await axios.get('getallagents');
  return response.data;
};

/**
 * Get filter all agents existents
 * @returns {Promise}
 */
Agent.getFilterAgents = async (
  name_agent,
  subgroup,
  modality,
  acessor,
  class_agent,
  subclass
) => {
  const params = {
    name_agent: name_agent,
    subgroup: subgroup,
    modality: modality,
    acessor: acessor,
    class_agent: class_agent,
    subclass: subclass,
  };
  const response = await axios.get('getfilteragents', { params: params });
  return response.data;
};

export default Agent;
