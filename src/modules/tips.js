import axios from './config';

const Tips = {};

/**
 * Get a random tip
 * @returns {Promise}
 */
Tips.getRandom = async () => {
  const response = await axios.get('getrandomtip');
  return response.data;
};

export default Tips;
