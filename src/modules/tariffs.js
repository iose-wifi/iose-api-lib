import axios from './config';

const Tariff = {};

/**
 * Get tarrifs data of an unity
 * @param {String} uuidUnity - unity identifier
 * @param {Array} circuits - circuits array
 * @param {String} startInterval - start date
 * @param {String} endInterval - end date
 * @returns
 */
Tariff.getTariffByUnity = async (
  uuidUnity,
  circuits,
  startInterval,
  endInterval,
) => {
  const circuitsString = JSON.stringify(circuits);

  const params = {
    uuid_unity: uuidUnity,
    circuits: circuitsString,
    startInterval: startInterval,
    endInterval: endInterval,
  };
  const response = await axios.get('getTariffBasedUnityType', {params: params});
  return response.data;
};

/**
 * Get tarrifs data of an unity with periods
 * @param {String} uuidUnity - unity identifier
 * @param {Array} circuits - circuits array
 * @param {String} startInterval - start date
 * @param {String} endInterval - end date
 * @param {String} modality - end date
 * @returns
 */
Tariff.getTariffBasedUnityTypeAndPeriod = async (
  uuidUnity,
  circuits,
  startInterval,
  endInterval,
  modality,
) => {
  const circuitsString = JSON.stringify(circuits);

  const params = {
    uuid_unity: uuidUnity,
    circuits: circuitsString,
    startInterval: startInterval,
    endInterval: endInterval,
    modality: modality,
  };
  const response = await axios.get('getTariffBasedUnityTypeAndPeriod', {
    params: params,
  });
  return response.data;
};

export default Tariff;
