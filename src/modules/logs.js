import axios from './config';

const Logs = {};

/**
 * Get all logs circuit
 * @param {String} uuid_circuit - client identifier
 * @param {Object} pageCode - (optional) code of the page - default: '0'
 * @param {number} pageSize - (optional) size of the page - default: 300
 * @return {Promise}
 */
Logs.getAllLogs = async (uuid_circuit, pageCode = '0', pageSize = 300) => {
  const params = {
    uuid_circuit: uuid_circuit,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getalllogs', { params: params });
  return response.data;
};

export default Logs;
