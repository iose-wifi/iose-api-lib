import axios from './config';

const CircuitThreePhase = {};

/**
 * Create a circuitThreePhase
 * @param {String} name - circuit three-phase name
 * @param {String} description - circuit three-phase  description
 * @param {String} identifier_phase_a - uuid circuit phase A
 * @param {String} identifier_phase_b - uuid circuit phase B
 * @param {String} identifier_phase_c - uuid circuit phase C
 * @param {String} uuid_group - uuid group of circuit
 * @param {String} uuid_unity - uuid unity of circuit
 * @param {String} uuid_client - uuid client of circuit
 * @return {Promise}
 */
CircuitThreePhase.create = async (
  name,
  description,
  identifier_phase_a,
  identifier_phase_b,
  identifier_phase_c,
  uuid_group,
  uuid_unity,
  uuid_client) => {
  
  const data = {
    name:name,
    description:description,
    identifier_phase_a:identifier_phase_a,
    identifier_phase_b:identifier_phase_b,
    identifier_phase_c:identifier_phase_c,
    uuid_group:uuid_group,
    uuid_unity:uuid_unity,
    uuid_client:uuid_client
  };

  const response = await axios.post('insertThreePhaseCircuit', data);
  return response.data;
};

/**
 * Get all circuits three-phase by group
 * @param {String} uuidGroup - uuid group 
 * @returns {Promise}
 */
 CircuitThreePhase.get = async (uuidGroup) => {

  const params = {
    uuid_group: uuidGroup,
  };

  const response = await axios.get('getThreePhaseCircuitsByGroup', { params: params });
  return response.data;
};

/**
 * Update a circuitThreePhase
 * @param {String} uuid_three_phase_circuit - circuit three-phase uuid
 * @param {String} name - circuit three-phase name
 * @param {String} description - circuit three-phase  description
 * @returns {Promise}
 */
 CircuitThreePhase.update = async (
  uuid_three_phase_circuit,
  name,
  description) => {
  
  let params = {
    uuid_three_phase_circuit:uuid_three_phase_circuit,
    name: name,
    description: description
  };

  const response = await axios.put('updateThreePhaseCircuit', params);
  return response.data;
};

/**
 * Delete a circuitThreePhase
 * @param {String} uuid_three_phase_circuit - circuit three-phase uuid
 * @param {String} identifier_phase_a - uuid circuit phase A
 * @param {String} identifier_phase_b - uuid circuit phase B
 * @param {String} identifier_phase_c - uuid circuit phase C
 * @returns {Promise}
 */
 CircuitThreePhase.delete = async (
  uuid_three_phase_circuit,
  identifier_phase_a,
  identifier_phase_b,
  identifier_phase_c) => {

  const params = {
    uuid_three_phase_circuit:uuid_three_phase_circuit,
    identifier_phase_a:identifier_phase_a,
    identifier_phase_b:identifier_phase_b,
    identifier_phase_c:identifier_phase_c
  };

  const response = await axios.delete('deleteThreePhaseCircuit', { data: params });
  return response.data;
};



export default CircuitThreePhase;
