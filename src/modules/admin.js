import axios from './config';

const Admin = {};

/**
 * Create a admin
 * @param {String} uuid_client - client identifier
 * @param {String} name - administrator name
 * @param {String} email - administrator  email
 * @return {Promise}
 */
Admin.createAdmin = async (uuid_client, name, email) => {
  const data = {
    name: name,
    email: email,
    uuid_client: uuid_client,
  };
  const response = await axios.post('createadmin', data);
  return response.data;
};

/**
 * Get data of an admin
 * @param {String} uuidAdmin -admin identifier
 * @returns {Promise}
 */
Admin.getAdmin = async (uuidAdmin) => {
  const params = {
    uuid_admin: uuidAdmin,
  };
  const response = await axios.get('getadmin', { params: params });
  return response.data;
};

/**
 * Get all admins of a client
 * @param {String} uuidClient - client identifier
 * @param {Object} pageCode - (optional) code of the page - default: '0'
 * @param {number} pageSize - (optional) size of the page - default: 300
 * @returns {Promise}
 */
Admin.getAllAdmin = async (uuidClient, pageCode = '0', pageSize = 300) => {
  const params = {
    uuid_client: uuidClient,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getalladmin', { params: params });
  return response.data;
};

/**
 * Delete an admin
 * @param {String} uuidAdmin - admin identifier
 * @returns {Promise}
 */
Admin.deleteAdmin = async (uuidAdmin) => {
  const params = {
    uuid_admin: uuidAdmin,
  };

  const response = await axios.delete('deleteadmin', { data: params });
  return response.data;
};

/**
 * Update an admin
 * @param {String} uuidAdmin - admin identifier
 * @param {String} name -admin name
 * @param {String} email -admin email
 * @returns {Promise}
 */
Admin.updateAdmin = async ({
  uuidAdmin,
  name,
  cep,
  cpf,
  rua,
  numero,
  bairro,
  cidade,
  estado,
}) => {
  let params = {
    uuid_admin: uuidAdmin,
    name: name,
    cep: cep,
    cpf: cpf,
    rua: rua,
    numero: numero,
    bairro: bairro,
    cidade: cidade,
    estado: estado,
  };
  const response = await axios.put('updateadmin', params);
  return response.data;
};

export default Admin;
