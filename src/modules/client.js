import axios from './config';

const Client = {};

/**
 * Create a client
 * @param {String} name - company name
 * @param {String} description - company  description
 * @return {Promise}
 */
Client.create = async (name, description) => {
  const data = {
    company_name: name,
    company_description: description,
  };
  const response = await axios.post('createclient', data);
  return response.data;
};

/**
 * Get data of a client
 * @param {String} uuidClient - client identifier
 * @returns {Promise}
 */
Client.get = async (uuidClient) => {
  const params = {
    uuid_client: uuidClient,
  };
  const response = await axios.get('getclient', { params: params });
  return response.data;
};

/**
 * Get all clients
 * @param {Object} pageCode - (optional) code of the page - default: '0'
 * @param {number} pageSize - (optional) size of the page - default: 300
 * @returns {Promise}
 */
Client.getAll = async (pageCode = '0', pageSize = 300) => {
  const params = {
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getallclient', { params: params });
  return response.data;
};

/**
 * Delete a client
 * @param {String} uuidClient - client identifier
 * @returns {Promise}
 */
Client.delete = async (uuidClient) => {
  const params = {
    uuid_client: uuidClient,
  };

  const response = await axios.delete('deleteclient', { data: params });
  return response.data;
};

/**
 * Update a client
 * @param {String} uuidClient - client identifier
 * @param {String} name -client name
 * @param {String} description -client description
 * @returns {Promise}
 */
Client.update = async (uuidClient, name, description) => {
  let params = {
    uuid_client: uuidClient,
    name: name,
    description: description,
  };
  const response = await axios.put('updateclient', params);
  return response.data;
};

export default Client;
