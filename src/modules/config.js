import axios from 'axios';

const PATH_PRODUCTION =
  'https://0kp0y3fg13.execute-api.us-east-1.amazonaws.com/prod/';
const PATH_DEVELOPMENT =
  'https://vsnhd5rs46.execute-api.us-west-2.amazonaws.com/dev/';

axios.defaults.baseURL =
  process.env.REACT_APP_ENV === 'prod' ? PATH_PRODUCTION : PATH_DEVELOPMENT;

export const setEnv = (env = 'dev') => {
  const path =
    env === 'prod' || process.env.REACT_APP_ENV === 'prod'
      ? PATH_PRODUCTION
      : PATH_DEVELOPMENT;
  axios.defaults.baseURL = path;
};

export const setToken = (token) => {
  axios.defaults.headers.common.Authorization = token;
};

export const showHeader = () => {
  console.log(axios.defaults.headers.common);
};

export const showUrl = () => {
  console.log(axios.defaults.baseURL);
};

export default axios;
