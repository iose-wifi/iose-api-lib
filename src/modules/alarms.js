import axios from './config';

const Alarms = {};

/**
 * Get all alarms of a circult between start date and end date
 * @param {String} uuidCircuit - circuit identifier
 * @param {String} startDate - start date
 * @param {String} endDate - end date
 * @param {Object} pageCode - code of the page
 * @param {Number} pageSize - size of the page
 * @returns {Promise}
 */
Alarms.getAllAlarmsByCircuitBetweenDates = async (
  uuidCircuit,
  startDate,
  endDate,
  pageCode = '0',
  pageSize = 300
) => {
  const params = {
    uuid_circuit: uuidCircuit,
    start_date: startDate,
    end_date: endDate,
    page_code: pageCode,
    page_size: pageSize,
  };
  const response = await axios.get('getallalarmsbycircuitbetweendates', {
    params: params,
  });

  return response.data;
};

export default Alarms;
