import { setToken, showHeader, showUrl, setEnv } from './modules/config';

import Admin from './modules/admin';
import Employee from './modules/employee';
import Circuit from './modules/circuit';
import Unity from './modules/unity';
import Group from './modules/group';
import Tariff from './modules/tariffs';
import Agent from './modules/agent';
import Flag from './modules/flag';
import Measures from './modules/measures';
import HouseholdAppliance from './modules/household_appliance';
import Cep from './modules/cep';
import Notifications from './modules/notifications';
import Tips from './modules/tips';
import Client from './modules/client';
import Logs from './modules/logs';
import Alarms from './modules/alarms';
import CircuitThreePhase from './modules/circuitThreePhase' 

const IoseApiLib = {
  setToken,
  showHeader,
  setEnv,
  showUrl,
  Admin,
  Employee,
  Circuit,
  Unity,
  Group,
  Tariff,
  Agent,
  Flag,
  Measures,
  HouseholdAppliance,
  Cep,
  Notifications,
  Tips,
  Client,
  Logs,
  Alarms,
  CircuitThreePhase
};

export default IoseApiLib;
